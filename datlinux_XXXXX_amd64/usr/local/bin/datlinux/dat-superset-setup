#!/bin/bash

echo -e "\e[1;36m∞ Superset set-up required. \e[0;m"

if [ ! -f "/home/louis/.pyenv/versions/3.9.13/bin/python3.9" ]; then
    echo "
⚠️  Apache Superset has compatibility issues with Python-3.10. ( Please check 
the DAT Linux announcements page for any updates on this issue: 
https://github.com/dat-linux/community/discussions/categories/announcements )

In order to proceed with Superset, Python-3.9 must be installed concurrently.
"
    read -p "Proceed with Python-3.9 install? [Y|n]: " input
    if [[ $input != "Y" && $input != "y" && $input != "" ]]; then
        exit 0
    fi
    if [ ! -f $HOME/.pyenv/bin/pyenv ]; then
        git clone https://github.com/pyenv/pyenv.git $HOME/.pyenv
    fi
    echo -e "\e[0;36mInstalling Python-3.9, please be patient.. \e[0;m"
    $HOME/.pyenv/bin/pyenv install 3.9.13
fi

read -p "Proceed with Superset install? [Y|n]: " input
if [[ $input != "Y" && $input != "y" && $input != "" ]]; then
    exit 0
fi

. /usr/local/bin/datlinux/dat-ping
check_internet || check_internet_err_msg

echo -e "\e[0;36mFirst time set-up for Superset, please be patient.. \e[0;m"
echo -e "\e[0;36m(!! sudo password may be required for this install.) \e[0;m"
echo -e "\e[0;36m(!! Please follow any prompts.) \e[0;m"

. $HOME/.cari/cari.sh
. $HOME/.cari/completions/cari.bash
cari add superset || cari update superset
cari install superset latest
cari global superset latest

if ! compgen -G "$HOME/.cari/venv/superset/*/bin/superset" > /dev/null ||
    [ ! -f "$HOME/.cari/shims/superset" ]; then
    echo -e "\e[0;36mSuperset set-up failed!\e[0;m"
    cari uninstall superset
    sleep 5
    exit 1
fi
    
echo -e "\e[0;36mSuperset set-up successful! (`cari which superset`)\e[0;m"
echo '----------------------------------------------'
echo -e "Access via browser: \e[0;36mhtp://localhost:8088\e[0;m"
echo "To run from a terminal:"
echo -e "\e[0;36msource '$HOME/.cari/venv/superset/bin/activate' && superset\e[0;m"
echo "Or add an alias to $HOME/.bashrc: "
echo -e "\e[0;36malias superset=\"source '$HOME/.cari/venv/superset/bin/activate'\"\e[0;m"
echo -e "\e[0;36malias superset=\"source '$HOME/.cari/venv/superset/bin/superset run -p 8088 --with-threads --reload --debugger' && superset\"\e[0;m"
echo "Don't forget to: "
echo -e "\e[0;36mdeactivate\e[0;m"
echo '----------------------------------------------'
sleep 3
/usr/local/bin/datlinux/dat-superset-options

