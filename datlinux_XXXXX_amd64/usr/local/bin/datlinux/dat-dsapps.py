#!/usr/bin/python3

import os
import re
import socket
import sys
import time
import locale
import threading
import textwrap
import configparser
import tkinter as tk
import tkinter.font as font
import tkinter.ttk as ttk
from functools import partial
from pathlib import Path
from PIL import Image, ImageTk
from idlelib.tooltip import Hovertip
from ttkthemes import ThemedTk
from threading import Thread
from threading import Event

LOCK_PROC = 'dsappspy'
REPO = 'https://gitlab.com/datlinux/control-panel-1.0.1-lxqt'
CTRLPNLVER='1.0.144-lxqt'

def get_lock(process_name):
    get_lock._lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        get_lock._lock_socket.bind('\0' + process_name)
        print('I got the lock')
    except socket.error:
        print('lock exists')
        sys.exit()

get_lock(LOCK_PROC)


def system_lang():
    default, _ = locale.getdefaultlocale()
    lan = default.split("_")[0]
    return lan

tr = {}
tr['af_ab'] = "Oor"
tr['sq_ab'] = "Rreth"
tr['am_ab'] = "ስለ"
tr['ar_ab'] = "حول"
tr['hy_ab'] = "Մասին"
tr['az_ab'] = "Haqqında"
tr['eu_ab'] = "Buruz"
tr['be_ab'] = "Аб"
tr['bn_ab'] = "সম্পর্কিত"
tr['bs_ab'] = "O"
tr['bg_ab'] = "относно"
tr['ca_ab'] = "Sobre"
tr['ceb_ab'] = "Mahitungod sa"
tr['ny_ab'] = "Za"
tr['zh_ab'] = "關於"
tr['co_ab'] = "À propositu"
tr['hr_ab'] = "Oko"
tr['cs_ab'] = "O"
tr['da_ab'] = "Om"
tr['nl_ab'] = "Over"
tr['en_ab'] = "About"
tr['eo_ab'] = "Pri"
tr['et_ab'] = "Umbes"
tr['tl_ab'] = "Tungkol sa"
tr['fi_ab'] = "Noin"
tr['fr_ab'] = "À propos de"
tr['fy_ab'] = "Oer"
tr['gl_ab'] = "Sobre"
tr['ka_ab'] = "შესახებ"
tr['de_ab'] = "Um"
tr['el_ab'] = "Σχετικά με"
tr['gu_ab'] = "વિશે"
tr['ht_ab'] = "Konsènan"
tr['ha_ab'] = "Game da"
tr['haw_ab'] = "E pili ana"
tr['iw_ab'] = "על אודות"
tr['he_ab'] = "על אודות"
tr['hi_ab'] = "के बारे में"
tr['hmn_ab'] = "Txog"
tr['hu_ab'] = "Ról ről"
tr['is_ab'] = "Um"
tr['ig_ab'] = "Ihe gbasara"
tr['id_ab'] = "Tentang"
tr['ga_ab'] = "Faoi"
tr['it_ab'] = "Di"
tr['ja_ab'] = "約"
tr['jw_ab'] = "Babagan"
tr['kn_ab'] = "ಬಗ್ಗೆ"
tr['kk_ab'] = "туралы"
tr['km_ab'] = "អំពី"
tr['ko_ab'] = "에 대한"
tr['ku_ab'] = "Ji dor"
tr['ky_ab'] = "Жөнүндө"
tr['lo_ab'] = "ກ່ຽວກັບ"
tr['la_ab'] = "De"
tr['lv_ab'] = "Par"
tr['lt_ab'] = "Apie"
tr['lb_ab'] = "Iwwer"
tr['mk_ab'] = "За"
tr['mg_ab'] = "About"
tr['ms_ab'] = "Tentang"
tr['ml_ab'] = "കുറിച്ച്"
tr['mt_ab'] = "Dwar"
tr['mi_ab'] = "Mō"
tr['mr_ab'] = "बद्दल"
tr['mn_ab'] = "тухай"
tr['my_ab'] = "အကြောင်း"
tr['ne_ab'] = "बारे"
tr['no_ab'] = "Om"
tr['or_ab'] = "ବିଷୟରେ"
tr['ps_ab'] = "په اړه"
tr['fa_ab'] = "در باره"
tr['pl_ab'] = "O"
tr['pt_ab'] = "Sobre"
tr['pa_ab'] = "ਬਾਰੇ"
tr['ro_ab'] = "Despre"
tr['ru_ab'] = "О"
tr['sm_ab'] = "E uiga i"
tr['gd_ab'] = "Mu dheidhinn"
tr['sr_ab'] = "О томе"
tr['st_ab'] = "About"
tr['sn_ab'] = "About"
tr['sd_ab'] = "بابت"
tr['si_ab'] = "ගැන"
tr['sk_ab'] = "O"
tr['sl_ab'] = "O tem"
tr['so_ab'] = "Ku saabsan"
tr['es_ab'] = "Sobre"
tr['su_ab'] = "Ngeunaan"
tr['sw_ab'] = "Kuhusu"
tr['sv_ab'] = "Handla om"
tr['tg_ab'] = "Дар бораи"
tr['ta_ab'] = "பற்றி"
tr['te_ab'] = "గురించి"
tr['th_ab'] = "เกี่ยวกับ"
tr['tr_ab'] = "Hakkında"
tr['uk_ab'] = "про"
tr['ur_ab'] = "کے بارے میں"
tr['ug_ab'] = "ھەققىدە"
tr['uz_ab'] = "Haqida"
tr['vi_ab'] = "Về"
tr['cy_ab'] = "Ynghylch"
tr['xh_ab'] = "Malunga"
tr['yi_ab'] = "וועגן"
tr['yo_ab'] = "Nipa"
tr['zu_ab'] = "Mayelana"
tr['af_di'] = "Verspreiding"
tr['sq_di'] = "Shpërndarja"
tr['am_di'] = "ስርጭት"
tr['ar_di'] = "توزيع"
tr['hy_di'] = "Բաշխում"
tr['az_di'] = "Paylanma"
tr['eu_di'] = "Banaketa"
tr['be_di'] = "Размеркаванне"
tr['bn_di'] = "বিতরণ"
tr['bs_di'] = "Distribucija"
tr['bg_di'] = "Разпределение"
tr['ca_di'] = "Distribució"
tr['ceb_di'] = "Pag-apod-apod"
tr['ny_di'] = "Kugawa"
tr['zh_di'] = "分配"
tr['co_di'] = "Distribuzione"
tr['hr_di'] = "Distribucija"
tr['cs_di'] = "Rozdělení"
tr['da_di'] = "Fordeling"
tr['nl_di'] = "Verdeling"
tr['en_di'] = "Distribution"
tr['eo_di'] = "Distribuo"
tr['et_di'] = "Levitamine"
tr['tl_di'] = "Pamamahagi"
tr['fi_di'] = "Jakelu"
tr['fr_di'] = "Distribution"
tr['fy_di'] = "Distribúsje"
tr['gl_di'] = "Distribución"
tr['ka_di'] = "დისტრიბუცია"
tr['de_di'] = "Verteilung"
tr['el_di'] = "Διανομή"
tr['gu_di'] = "વિતરણ"
tr['ht_di'] = "Distribisyon"
tr['ha_di'] = "Rarrabawa"
tr['haw_di'] = "Hoʻolaha"
tr['iw_di'] = "הפצה"
tr['he_di'] = "הפצה"
tr['hi_di'] = "वितरण"
tr['hmn_di'] = "Kev faib tawm"
tr['hu_di'] = "terjesztés"
tr['is_di'] = "Dreifing"
tr['ig_di'] = "Nkesa"
tr['id_di'] = "Distribusi"
tr['ga_di'] = "Dáileadh"
tr['it_di'] = "Distribuzione"
tr['ja_di'] = "分布"
tr['jw_di'] = "Distribusi"
tr['kn_di'] = "ವಿತರಣೆ"
tr['kk_di'] = "Тарату"
tr['km_di'] = "ការចែកចាយ"
tr['ko_di'] = "분포"
tr['ku_di'] = "Belavkirinî"
tr['ky_di'] = "Бөлүштүрүү"
tr['lo_di'] = "ການແຜ່ກະຈາຍ"
tr['la_di'] = "Distributio"
tr['lv_di'] = "Izplatīšana"
tr['lt_di'] = "Paskirstymas"
tr['lb_di'] = "Verdeelung"
tr['mk_di'] = "Дистрибуција"
tr['mg_di'] = "fizarana"
tr['ms_di'] = "Pengagihan"
tr['ml_di'] = "വിതരണ"
tr['mt_di'] = "Distribuzzjoni"
tr['mi_di'] = "Tohanga"
tr['mr_di'] = "वितरण"
tr['mn_di'] = "Хуваарилалт"
tr['my_di'] = "ဖြန့်ဝေခြင်း။"
tr['ne_di'] = "वितरण"
tr['no_di'] = "Fordeling"
tr['or_di'] = "ବଣ୍ଟନ"
tr['ps_di'] = "ویش"
tr['fa_di'] = "توزیع"
tr['pl_di'] = "Dystrybucja"
tr['pt_di'] = "Distribuição"
tr['pa_di'] = "ਵੰਡ"
tr['ro_di'] = "Distributie"
tr['ru_di'] = "Распределение"
tr['sm_di'] = "Fa'asoa"
tr['gd_di'] = "Sgaoileadh"
tr['sr_di'] = "Дистрибуција"
tr['st_di'] = "Kabo"
tr['sn_di'] = "Distribution"
tr['sd_di'] = "ورهائڻ"
tr['si_di'] = "බෙදා හැරීම"
tr['sk_di'] = "Distribúcia"
tr['sl_di'] = "Distribucija"
tr['so_di'] = "Qaybinta"
tr['es_di'] = "Distribución"
tr['su_di'] = "Distribusi"
tr['sw_di'] = "Usambazaji"
tr['sv_di'] = "Distribution"
tr['tg_di'] = "Тақсим"
tr['ta_di'] = "விநியோகம்"
tr['te_di'] = "పంపిణీ"
tr['th_di'] = "การกระจาย"
tr['tr_di'] = "Dağıtım"
tr['uk_di'] = "Розподіл"
tr['ur_di'] = "تقسیم"
tr['ug_di'] = "تارقىتىش"
tr['uz_di'] = "Tarqatish"
tr['vi_di'] = "Phân bổ"
tr['cy_di'] = "Dosbarthiad"
tr['xh_di'] = "Ukusasazwa"
tr['yi_di'] = "פאַרשפּרייטונג"
tr['yo_di'] = "Pinpin"
tr['zu_di'] = "Ukusabalalisa"
tr['af_cp'] = "Beheer paneel"
tr['sq_cp'] = "Paneli i kontrollit"
tr['am_cp'] = "መቆጣጠሪያ ሰሌዳ"
tr['ar_cp'] = "لوحة التحكم"
tr['hy_cp'] = "Կառավարման վահանակ"
tr['az_cp'] = "İdarə paneli"
tr['eu_cp'] = "Kontrol panela"
tr['be_cp'] = "Панэль кіравання"
tr['bn_cp'] = "কন্ট্রোল প্যানেল"
tr['bs_cp'] = "Kontrolna tabla"
tr['bg_cp'] = "Контролен панел"
tr['ca_cp'] = "Panell de control"
tr['ceb_cp'] = "Control Panel"
tr['ny_cp'] = "Gawo lowongolera"
tr['zh_cp'] = "控制面板"
tr['co_cp'] = "Panel di cuntrollu"
tr['hr_cp'] = "Upravljačka ploča"
tr['cs_cp'] = "Kontrolní panel"
tr['da_cp'] = "Kontrolpanel"
tr['nl_cp'] = "Controlepaneel"
tr['en_cp'] = "Control Panel"
tr['eo_cp'] = "Kontrola Panelo"
tr['et_cp'] = "Kontrollpaneel"
tr['tl_cp'] = "Control Panel"
tr['fi_cp'] = "Ohjauspaneeli"
tr['fr_cp'] = "Panneau de commande"
tr['fy_cp'] = "Control Panel"
tr['gl_cp'] = "Panel de control"
tr['ka_cp'] = "Მართვის პანელი"
tr['de_cp'] = "Schalttafel"
tr['el_cp'] = "Πίνακας Ελέγχου"
tr['gu_cp'] = "કંટ્રોલ પેનલ"
tr['ht_cp'] = "Kontwòl Panel"
tr['ha_cp'] = "Kwamitin Kulawa"
tr['haw_cp'] = "Panel Manao"
tr['iw_cp'] = "לוח בקרה"
tr['he_cp'] = "לוח בקרה"
tr['hi_cp'] = "कंट्रोल पैनल"
tr['hmn_cp'] = "Tswj Vaj Huam Sib Luag"
tr['hu_cp'] = "Vezérlőpult"
tr['is_cp'] = "Stjórnborð"
tr['ig_cp'] = "Ogwe njikwa"
tr['id_cp'] = "Panel kendali"
tr['ga_cp'] = "Painéal rialú"
tr['it_cp'] = "Pannello di controllo"
tr['ja_cp'] = "コントロールパネル"
tr['jw_cp'] = "Panel Kontrol"
tr['kn_cp'] = "ನಿಯಂತ್ರಣಫಲಕ"
tr['kk_cp'] = "Басқару панелі"
tr['km_cp'] = "ផ្ទាំងបញ្ជា"
tr['ko_cp'] = "제어판"
tr['ku_cp'] = "Panela Kontrolê"
tr['ky_cp'] = "Башкаруу панели"
tr['lo_cp'] = "ກະດານຄວບຄຸມ"
tr['la_cp'] = "Imperium Panel"
tr['lv_cp'] = "Vadības panelis"
tr['lt_cp'] = "Kontrolės skydelis"
tr['lb_cp'] = "Kontroll Panel"
tr['mk_cp'] = "Контролен панел"
tr['mg_cp'] = "Takelaka fikirakirana"
tr['ms_cp'] = "Panel kawalan"
tr['ml_cp'] = "നിയന്ത്രണ പാനൽ"
tr['mt_cp'] = "Panel tal-Kontroll"
tr['mi_cp'] = "Paewhiri Mana"
tr['mr_cp'] = "नियंत्रण पॅनेल"
tr['mn_cp'] = "Хяналтын самбар"
tr['my_cp'] = "ထိန်းချုပ်ရာနေရာ"
tr['ne_cp'] = "नियन्त्रण कक्ष"
tr['no_cp'] = "Kontrollpanel"
tr['or_cp'] = "କଣ୍ଟ୍ରୋଲ୍ ପ୍ୟାନେଲ୍ |"
tr['ps_cp'] = "د تضمین اداره"
tr['fa_cp'] = "صفحه کنترل"
tr['pl_cp'] = "Panel sterowania"
tr['pt_cp'] = "Painel de controle"
tr['pa_cp'] = "ਕਨ੍ਟ੍ਰੋਲ ਪੈਨਲ"
tr['ro_cp'] = "Panou de control"
tr['ru_cp'] = "Панель управления"
tr['sm_cp'] = "Vaega Pule"
tr['gd_cp'] = "Pannal Smachd"
tr['sr_cp'] = "Контролна табла"
tr['st_cp'] = "Lekhotla la taolo"
tr['sn_cp'] = "Control Panel"
tr['sd_cp'] = "ڪنٽرول پينل"
tr['si_cp'] = "පාලන පුවරුව"
tr['sk_cp'] = "Ovládací panel"
tr['sl_cp'] = "Nadzorna plošča"
tr['so_cp'] = "Guddiga xakamaynta"
tr['es_cp'] = "Panel de control"
tr['su_cp'] = "Panél kontrol"
tr['sw_cp'] = "Jopo kudhibiti"
tr['sv_cp'] = "Kontrollpanel"
tr['tg_cp'] = "Сафҳаи идоракунӣ"
tr['ta_cp'] = "கண்ட்ரோல் பேனல்"
tr['te_cp'] = "నియంత్రణ ప్యానెల్"
tr['th_cp'] = "แผงควบคุม"
tr['tr_cp'] = "Kontrol Paneli"
tr['uk_cp'] = "Панель управління"
tr['ur_cp'] = "کنٹرول پینل"
tr['ug_cp'] = "كونترول تاختىسى"
tr['uz_cp'] = "Boshqaruv paneli"
tr['vi_cp'] = "Bảng điều khiển"
tr['cy_cp'] = "Panel Rheoli"
tr['xh_cp'] = "Iphaneli yokulawula"
tr['yi_cp'] = "קאָנטראָל פּאַנעל"
tr['yo_cp'] = "Ibi iwaju alabujuto"
tr['zu_cp'] = "Iphaneli yokulawula"
tr['af_ds_1'] = "Datawetenskap"
tr['sq_ds_1'] = "Shkenca e të Dhënave"
tr['am_ds_1'] = "የውሂብ ሳይንስ"
tr['ar_ds_1'] = "علم البيانات"
tr['hy_ds_1'] = "Տվյալների գիտություն"
tr['az_ds_1'] = "Məlumat Elmi"
tr['eu_ds_1'] = "Datuen Zientzia"
tr['be_ds_1'] = "Навука аб дадзеных"
tr['bn_ds_1'] = "ডেটা সায়েন্স"
tr['bs_ds_1'] = "Data Science"
tr['bg_ds_1'] = "Наука за данни"
tr['ca_ds_1'] = "Ciència de dades"
tr['ceb_ds_1'] = "Data Science"
tr['ny_ds_1'] = "Sayansi ya Data"
tr['zh_ds_1'] = "數據科學"
tr['co_ds_1'] = "Data Science"
tr['hr_ds_1'] = "Znanost o podacima"
tr['cs_ds_1'] = "Data Science"
tr['da_ds_1'] = "Datavidenskab"
tr['nl_ds_1'] = "Gegevenswetenschap"
tr['en_ds_1'] = "Data Science"
tr['eo_ds_1'] = "Datumscienco"
tr['et_ds_1'] = "Andmeteadus"
tr['tl_ds_1'] = "Agham ng Data"
tr['fi_ds_1'] = "Tietotiede"
tr['fr_ds_1'] = "Science des données"
tr['fy_ds_1'] = "Data Science"
tr['gl_ds_1'] = "Ciencia de datos"
tr['ka_ds_1'] = "მონაცემთა მეცნიერება"
tr['de_ds_1'] = "Datenwissenschaft"
tr['el_ds_1'] = "Επιστημονικά δεδομένα"
tr['gu_ds_1'] = "ડેટા સાયન્સ"
tr['ht_ds_1'] = "Syans done"
tr['ha_ds_1'] = "Kimiyyar Bayanai"
tr['haw_ds_1'] = "ʻEpekema ʻIkepili"
tr['iw_ds_1'] = "מדע נתונים"
tr['he_ds_1'] = "מדע נתונים"
tr['hi_ds_1'] = "डेटा विज्ञान"
tr['hmn_ds_1'] = "Data Science"
tr['hu_ds_1'] = "Adattudomány"
tr['is_ds_1'] = "Gagnafræði"
tr['ig_ds_1'] = "Sayensị data"
tr['id_ds_1'] = "Ilmu Data"
tr['ga_ds_1'] = "Eolaíocht Sonraí"
tr['it_ds_1'] = "Scienza dei dati"
tr['ja_ds_1'] = "データサイエンス"
tr['jw_ds_1'] = "Ilmu Data"
tr['kn_ds_1'] = "ಡೇಟಾ ಸೈನ್ಸ್"
tr['kk_ds_1'] = "Деректер туралы ғылым"
tr['km_ds_1'] = "វិទ្យាសាស្ត្រទិន្នន័យ"
tr['ko_ds_1'] = "데이터 과학"
tr['ku_ds_1'] = "Daneyên Zanistî"
tr['ky_ds_1'] = "Data Science"
tr['lo_ds_1'] = "ວິທະຍາສາດຂໍ້ມູນ"
tr['la_ds_1'] = "Data Scientiae"
tr['lv_ds_1'] = "Datu zinātne"
tr['lt_ds_1'] = "Duomenų mokslas"
tr['lb_ds_1'] = "Data Science"
tr['mk_ds_1'] = "Наука за податоци"
tr['mg_ds_1'] = "Data Science"
tr['ms_ds_1'] = "Sains Data"
tr['ml_ds_1'] = "ഡാറ്റ സയൻസ്"
tr['mt_ds_1'] = "Xjenza tad-Data"
tr['mi_ds_1'] = "Pūtaiao Raraunga"
tr['mr_ds_1'] = "डेटा सायन्स"
tr['mn_ds_1'] = "Өгөгдлийн шинжлэх ухаан"
tr['my_ds_1'] = "ဒေတာသိပ္ပံ"
tr['ne_ds_1'] = "डाटा विज्ञान"
tr['no_ds_1'] = "Datavitenskap"
tr['or_ds_1'] = "ଡାଟା ସାଇନ୍ସ |"
tr['ps_ds_1'] = "د معلوماتو ساینس"
tr['fa_ds_1'] = "علم داده"
tr['pl_ds_1'] = "Nauka o danych"
tr['pt_ds_1'] = "Ciência de dados"
tr['pa_ds_1'] = "ਡਾਟਾ ਸਾਇੰਸ"
tr['ro_ds_1'] = "Știința datelor"
tr['ru_ds_1'] = "Наука о данных"
tr['sm_ds_1'] = "Saienisi Fa'amatalaga"
tr['gd_ds_1'] = "Saidheans Dàta"
tr['sr_ds_1'] = "Дата Сциенце"
tr['st_ds_1'] = "Saense ea Boitsebiso"
tr['sn_ds_1'] = "Data Science"
tr['sd_ds_1'] = "ڊيٽا سائنس"
tr['si_ds_1'] = "දත්ත විද්යාව"
tr['sk_ds_1'] = "Data Science"
tr['sl_ds_1'] = "Podatkovna znanost"
tr['so_ds_1'] = "Sayniska Xogta"
tr['es_ds_1'] = "Ciencia de los datos"
tr['su_ds_1'] = "Élmu Data"
tr['sw_ds_1'] = "Sayansi ya Data"
tr['sv_ds_1'] = "Datavetenskap"
tr['tg_ds_1'] = "Илми маълумот"
tr['ta_ds_1'] = "தரவு அறிவியல்"
tr['te_ds_1'] = "డేటా సైన్స్"
tr['th_ds_1'] = "วิทยาศาสตร์ข้อมูล"
tr['tr_ds_1'] = "Veri Bilimi"
tr['uk_ds_1'] = "Data Science"
tr['ur_ds_1'] = "ڈیٹا سائنس"
tr['ug_ds_1'] = "Data Science"
tr['uz_ds_1'] = "Ma'lumotlar fan"
tr['vi_ds_1'] = "Khoa học dữ liệu"
tr['cy_ds_1'] = "Gwyddor Data"
tr['xh_ds_1'] = "Inzululwazi yeDatha"
tr['yi_ds_1'] = "דאַטאַ וויסנשאַפֿט"
tr['yo_ds_1'] = "Data Imọ"
tr['zu_ds_1'] = "Isayensi Yedatha"
tr['af_ds_2'] = "Toepassings"
tr['sq_ds_2'] = "Aplikacionet"
tr['am_ds_2'] = "መተግበሪያዎች"
tr['ar_ds_2'] = "تطبيقات"
tr['hy_ds_2'] = "Հավելվածներ"
tr['az_ds_2'] = "Proqramlar"
tr['eu_ds_2'] = "Aplikazioak"
tr['be_ds_2'] = "Праграмы"
tr['bn_ds_2'] = "অ্যাপস"
tr['bs_ds_2'] = "Apps"
tr['bg_ds_2'] = "Приложения"
tr['ca_ds_2'] = "Aplicacions"
tr['ceb_ds_2'] = "Aplikasyon"
tr['ny_ds_2'] = "Mapulogalamu"
tr['zh_ds_2'] = "應用"
tr['co_ds_2'] = "Apps"
tr['hr_ds_2'] = "aplikacije"
tr['cs_ds_2'] = "Aplikace"
tr['da_ds_2'] = "Apps"
tr['nl_ds_2'] = "Apps"
tr['en_ds_2'] = "Apps"
tr['eo_ds_2'] = "Aplikoj"
tr['et_ds_2'] = "Rakendused"
tr['tl_ds_2'] = "Mga app"
tr['fi_ds_2'] = "Sovellukset"
tr['fr_ds_2'] = "applications"
tr['fy_ds_2'] = "Apps"
tr['gl_ds_2'] = "Aplicacións"
tr['ka_ds_2'] = "აპები"
tr['de_ds_2'] = "Anwendungen"
tr['el_ds_2'] = "Εφαρμογές"
tr['gu_ds_2'] = "એપ્સ"
tr['ht_ds_2'] = "Aplikasyon yo"
tr['ha_ds_2'] = "Aikace-aikace"
tr['haw_ds_2'] = "Apps"
tr['iw_ds_2'] = "אפליקציות"
tr['he_ds_2'] = "אפליקציות"
tr['hi_ds_2'] = "ऐप्स"
tr['hmn_ds_2'] = "Apps"
tr['hu_ds_2'] = "Alkalmazások elemre"
tr['is_ds_2'] = "Forrit"
tr['ig_ds_2'] = "Ngwa"
tr['id_ds_2'] = "Aplikasi"
tr['ga_ds_2'] = "Feidhmchláir"
tr['it_ds_2'] = "App"
tr['ja_ds_2'] = "アプリ"
tr['jw_ds_2'] = "Aplikasi"
tr['kn_ds_2'] = "ಅಪ್ಲಿಕೇಶನ್ಗಳು"
tr['kk_ds_2'] = "Қолданбалар"
tr['km_ds_2'] = "កម្មវិធី"
tr['ko_ds_2'] = "앱"
tr['ku_ds_2'] = "Apps"
tr['ky_ds_2'] = "Колдонмолор"
tr['lo_ds_2'] = "ແອັບ"
tr['la_ds_2'] = "Apps"
tr['lv_ds_2'] = "Lietotnes"
tr['lt_ds_2'] = "Programėlės"
tr['lb_ds_2'] = "Apps"
tr['mk_ds_2'] = "Апликации"
tr['mg_ds_2'] = "app"
tr['ms_ds_2'] = "Apl"
tr['ml_ds_2'] = "ആപ്പുകൾ"
tr['mt_ds_2'] = "Apps"
tr['mi_ds_2'] = "Taupānga"
tr['mr_ds_2'] = "अॅप्स"
tr['mn_ds_2'] = "Програмууд"
tr['my_ds_2'] = "အက်ပ်များ"
tr['ne_ds_2'] = "एपहरू"
tr['no_ds_2'] = "Apper"
tr['or_ds_2'] = "ଅନୁପ୍ରୟୋଗଗୁଡିକ"
tr['ps_ds_2'] = "ایپس"
tr['fa_ds_2'] = "برنامه ها"
tr['pl_ds_2'] = "Aplikacje"
tr['pt_ds_2'] = "Aplicativos"
tr['pa_ds_2'] = "ਐਪਸ"
tr['ro_ds_2'] = "Aplicații"
tr['ru_ds_2'] = "Программы"
tr['sm_ds_2'] = "Apps"
tr['gd_ds_2'] = "Apps"
tr['sr_ds_2'] = "Аппс"
tr['st_ds_2'] = "Lisebelisoa"
tr['sn_ds_2'] = "Apps"
tr['sd_ds_2'] = "ايپس"
tr['si_ds_2'] = "යෙදුම්"
tr['sk_ds_2'] = "Aplikácie"
tr['sl_ds_2'] = "Aplikacije"
tr['so_ds_2'] = "Apps"
tr['es_ds_2'] = "aplicaciones"
tr['su_ds_2'] = "Aplikasi"
tr['sw_ds_2'] = "Programu"
tr['sv_ds_2'] = "Appar"
tr['tg_ds_2'] = "Барномаҳо"
tr['ta_ds_2'] = "பயன்பாடுகள்"
tr['te_ds_2'] = "యాప్‌లు"
tr['th_ds_2'] = "แอพ"
tr['tr_ds_2'] = "Uygulamalar"
tr['uk_ds_2'] = "програми"
tr['ur_ds_2'] = "ایپس"
tr['ug_ds_2'] = "ئەپلەر"
tr['uz_ds_2'] = "Ilovalar"
tr['vi_ds_2'] = "Ứng dụng"
tr['cy_ds_2'] = "Apiau"
tr['xh_ds_2'] = "Usetyenziso"
tr['yi_ds_2'] = "אַפּפּס"
tr['yo_ds_2'] = "Awọn ohun elo"
tr['zu_ds_2'] = "Izinhlelo zokusebenza"
tr['af_ex'] = "Ekstras"
tr['sq_ex'] = "Shtesa"
tr['am_ex'] = "ተጨማሪዎች"
tr['ar_ex'] = "إضافات"
tr['hy_ex'] = "Հավելյալներ"
tr['az_ex'] = "Əlavələr"
tr['eu_ex'] = "Gehigarriak"
tr['be_ex'] = "Дадаткова"
tr['bn_ex'] = "অতিরিক্ত"
tr['bs_ex'] = "Extras"
tr['bg_ex'] = "Екстри"
tr['ca_ex'] = "Extres"
tr['ceb_ex'] = "Mga ekstra"
tr['ny_ex'] = "Zowonjezera"
tr['zh_ex'] = "附加功能"
tr['co_ex'] = "Extras"
tr['hr_ex'] = "Dodaci"
tr['cs_ex'] = "Extra"
tr['da_ex'] = "Ekstra"
tr['nl_ex'] = "Extra's"
tr['en_ex'] = "Extras"
tr['eo_ex'] = "Kromaĵoj"
tr['et_ex'] = "Lisad"
tr['tl_ex'] = "Mga extra"
tr['fi_ex'] = "Ekstrat"
tr['fr_ex'] = "Suppléments"
tr['fy_ex'] = "Extras"
tr['gl_ex'] = "Extras"
tr['ka_ex'] = "დამატებები"
tr['de_ex'] = "Extras"
tr['el_ex'] = "Πρόσθετα"
tr['gu_ex'] = "એક્સ્ટ્રાઝ"
tr['ht_ex'] = "Siplemantè"
tr['ha_ex'] = "Kari"
tr['haw_ex'] = "Nā mea hou aku"
tr['iw_ex'] = "תוספות"
tr['he_ex'] = "תוספות"
tr['hi_ex'] = "अतिरिक्त"
tr['hmn_ex'] = "Ntxiv"
tr['hu_ex'] = "Extrák"
tr['is_ex'] = "Aukahlutir"
tr['ig_ex'] = "Mgbakwunye"
tr['id_ex'] = "Ekstra"
tr['ga_ex'] = "Breiseáin"
tr['it_ex'] = "Extra"
tr['ja_ex'] = "エクストラ"
tr['jw_ex'] = "Ekstra"
tr['kn_ex'] = "ಎಕ್ಸ್ಟ್ರಾಗಳು"
tr['kk_ex'] = "Қосымшалар"
tr['km_ex'] = "បន្ថែម"
tr['ko_ex'] = "엑스트라"
tr['ku_ex'] = "Extras"
tr['ky_ex'] = "Кошумчалар"
tr['lo_ex'] = "ພິເສດ"
tr['la_ex'] = "Extras"
tr['lv_ex'] = "Ekstras"
tr['lt_ex'] = "Priedai"
tr['lb_ex'] = "Extras"
tr['mk_ex'] = "Додатоци"
tr['mg_ex'] = "Fanampiny"
tr['ms_ex'] = "Tambahan"
tr['ml_ex'] = "എക്സ്ട്രാകൾ"
tr['mt_ex'] = "Extras"
tr['mi_ex'] = "Tāpiritanga"
tr['mr_ex'] = "अवांतर"
tr['mn_ex'] = "Нэмэлт"
tr['my_ex'] = "အပိုတွေ"
tr['ne_ex'] = "अतिरिक्त"
tr['no_ex'] = "Ekstrautstyr"
tr['or_ex'] = "ଅତିରିକ୍ତ"
tr['ps_ex'] = "اضافي"
tr['fa_ex'] = "موارد اضافی"
tr['pl_ex'] = "Dodatki"
tr['pt_ex'] = "Extras"
tr['pa_ex'] = "ਵਾਧੂ"
tr['ro_ex'] = "In plus"
tr['ru_ex'] = "Дополнительно"
tr['sm_ex'] = "Fa'aopoopo"
tr['gd_ex'] = "Extras"
tr['sr_ex'] = "Ектрас"
tr['st_ex'] = "Keketso"
tr['sn_ex'] = "Extras"
tr['sd_ex'] = "اضافي"
tr['si_ex'] = "අමතර"
tr['sk_ex'] = "Extra"
tr['sl_ex'] = "Dodatki"
tr['so_ex'] = "Dheeraad ah"
tr['es_ex'] = "Extras"
tr['su_ex'] = "Tambahan"
tr['sw_ex'] = "Ziada"
tr['sv_ex'] = "Extrafunktioner"
tr['tg_ex'] = "Иловаҳо"
tr['ta_ex'] = "கூடுதல்"
tr['te_ex'] = "ఎక్స్‌ట్రాలు"
tr['th_ex'] = "ความพิเศษ"
tr['tr_ex'] = "Ekstralar"
tr['uk_ex'] = "Додатково"
tr['ur_ex'] = "اضافی"
tr['ug_ex'] = "قوشۇمچە"
tr['uz_ex'] = "Qo'shimchalar"
tr['vi_ex'] = "Bổ sung"
tr['cy_ex'] = "Ychwanegiadau"
tr['xh_ex'] = "Okongeziweyo"
tr['yi_ex'] = "עקסטראַס"
tr['yo_ex'] = "Awọn afikun"
tr['zu_ex'] = "Okungeziwe"
tr['af_ad'] = "admin"
tr['sq_ad'] = "Admin"
tr['am_ad'] = "አስተዳዳሪ"
tr['ar_ad'] = "مسؤل"
tr['hy_ad'] = "Ադմին"
tr['az_ad'] = "Admin"
tr['eu_ad'] = "Admin"
tr['be_ad'] = "Адмін"
tr['bn_ad'] = "অ্যাডমিন"
tr['bs_ad'] = "Admin"
tr['bg_ad'] = "Админ"
tr['ca_ad'] = "Admin"
tr['ceb_ad'] = "Admin"
tr['ny_ad'] = "Admin"
tr['zh_ad'] = "行政"
tr['co_ad'] = "Admin"
tr['hr_ad'] = "Administrator"
tr['cs_ad'] = "Admin"
tr['da_ad'] = "Admin"
tr['nl_ad'] = "beheerder"
tr['en_ad'] = "Admin"
tr['eo_ad'] = "Admin"
tr['et_ad'] = "Admin"
tr['tl_ad'] = "Admin"
tr['fi_ad'] = "Admin"
tr['fr_ad'] = "Administrateur"
tr['fy_ad'] = "Admin"
tr['gl_ad'] = "Admin"
tr['ka_ad'] = "ადმინ"
tr['de_ad'] = "Administrator"
tr['el_ad'] = "διαχειριστής"
tr['gu_ad'] = "એડમિન"
tr['ht_ad'] = "Admin"
tr['ha_ad'] = "Admin"
tr['haw_ad'] = "Admin"
tr['iw_ad'] = "מנהל מערכת"
tr['he_ad'] = "מנהל מערכת"
tr['hi_ad'] = "व्यवस्थापक"
tr['hmn_ad'] = "Admin"
tr['hu_ad'] = "Admin"
tr['is_ad'] = "Admin"
tr['ig_ad'] = "Admin"
tr['id_ad'] = "Admin"
tr['ga_ad'] = "Riarachán"
tr['it_ad'] = "amministratore"
tr['ja_ad'] = "管理者"
tr['jw_ad'] = "Admin"
tr['kn_ad'] = "ನಿರ್ವಾಹಕ"
tr['kk_ad'] = "Админ"
tr['km_ad'] = "អ្នកគ្រប់គ្រង"
tr['ko_ad'] = "관리자"
tr['ku_ad'] = "Admin"
tr['ky_ad'] = "Админ"
tr['lo_ad'] = "ບໍລິຫານ"
tr['la_ad'] = "Admin"
tr['lv_ad'] = "Administrators"
tr['lt_ad'] = "Admin"
tr['lb_ad'] = "Admin"
tr['mk_ad'] = "Админ"
tr['mg_ad'] = "Admin"
tr['ms_ad'] = "Admin"
tr['ml_ad'] = "അഡ്മിൻ"
tr['mt_ad'] = "Amministratur"
tr['mi_ad'] = "Kaiwhakahaere"
tr['mr_ad'] = "अॅडमिन"
tr['mn_ad'] = "админ"
tr['my_ad'] = "အက်မင်"
tr['ne_ad'] = "व्यवस्थापक"
tr['no_ad'] = "Admin"
tr['or_ad'] = "ଆଡମିନି"
tr['ps_ad'] = "اډمین"
tr['fa_ad'] = "مدیر"
tr['pl_ad'] = "Admin"
tr['pt_ad'] = "Administrador"
tr['pa_ad'] = "ਐਡਮਿਨ"
tr['ro_ad'] = "Admin"
tr['ru_ad'] = "Администратор"
tr['sm_ad'] = "Pule"
tr['gd_ad'] = "Rianachd"
tr['sr_ad'] = "Админ"
tr['st_ad'] = "Admin"
tr['sn_ad'] = "Admin"
tr['sd_ad'] = "ايڊمن"
tr['si_ad'] = "පරිපාලක"
tr['sk_ad'] = "Admin"
tr['sl_ad'] = "skrbnik"
tr['so_ad'] = "Admin"
tr['es_ad'] = "Administración"
tr['su_ad'] = "Admin"
tr['sw_ad'] = "Msimamizi"
tr['sv_ad'] = "Administration"
tr['tg_ad'] = "Админ"
tr['ta_ad'] = "நிர்வாகம்"
tr['te_ad'] = "అడ్మిన్"
tr['th_ad'] = "แอดมิน"
tr['tr_ad'] = "yönetici"
tr['uk_ad'] = "адмін"
tr['ur_ad'] = "ایڈمن"
tr['ug_ad'] = "باشقۇرغۇچى"
tr['uz_ad'] = "Admin"
tr['vi_ad'] = "Quản trị viên"
tr['cy_ad'] = "Gweinyddol"
tr['xh_ad'] = "Admin"
tr['yi_ad'] = "אַדמין"
tr['yo_ad'] = "Abojuto"
tr['zu_ad'] = "Admin"
tr['af_he'] = "Help"
tr['sq_he'] = "Ndihmë"
tr['am_he'] = "እገዛ"
tr['ar_he'] = "مساعدة"
tr['hy_he'] = "Օգնություն"
tr['az_he'] = "Kömək edin"
tr['eu_he'] = "Laguntza"
tr['be_he'] = "Даведка"
tr['bn_he'] = "সাহায্য"
tr['bs_he'] = "Upomoć"
tr['bg_he'] = "Помогне"
tr['ca_he'] = "Ajuda"
tr['ceb_he'] = "Tabang"
tr['ny_he'] = "Thandizeni"
tr['zh_he'] = "幫助"
tr['co_he'] = "Aiutu"
tr['hr_he'] = "Pomozite"
tr['cs_he'] = "Pomoc"
tr['da_he'] = "Hjælp"
tr['nl_he'] = "Helpen"
tr['en_he'] = "Help"
tr['eo_he'] = "Helpu"
tr['et_he'] = "Abi"
tr['tl_he'] = "Tulong"
tr['fi_he'] = "auta"
tr['fr_he'] = "Aider"
tr['fy_he'] = "Help"
tr['gl_he'] = "Axuda"
tr['ka_he'] = "დახმარება"
tr['de_he'] = "Hilfe"
tr['el_he'] = "Βοήθεια"
tr['gu_he'] = "મદદ"
tr['ht_he'] = "Ede"
tr['ha_he'] = "Taimako"
tr['haw_he'] = "Kokua"
tr['iw_he'] = "עֶזרָה"
tr['he_he'] = "עֶזרָה"
tr['hi_he'] = "मदद करना"
tr['hmn_he'] = "Pab"
tr['hu_he'] = "Segítség"
tr['is_he'] = "Hjálp"
tr['ig_he'] = "Enyemaka"
tr['id_he'] = "Membantu"
tr['ga_he'] = "Cabhrú"
tr['it_he'] = "Aiuto"
tr['ja_he'] = "ヘルプ"
tr['jw_he'] = "Pitulung"
tr['kn_he'] = "ಸಹಾಯ"
tr['kk_he'] = "Көмектесіңдер"
tr['km_he'] = "ជំនួយ"
tr['ko_he'] = "돕다"
tr['ku_he'] = "Alîkarî"
tr['ky_he'] = "Жардам"
tr['lo_he'] = "ຊ່ວຍເຫຼືອ"
tr['la_he'] = "Auxilium"
tr['lv_he'] = "Palīdzība"
tr['lt_he'] = "Pagalba"
tr['lb_he'] = "Hëllef"
tr['mk_he'] = "Помош"
tr['mg_he'] = "Vonjeo"
tr['ms_he'] = "Tolong"
tr['ml_he'] = "സഹായം"
tr['mt_he'] = "Għajnuna"
tr['mi_he'] = "Awhina"
tr['mr_he'] = "मदत करा"
tr['mn_he'] = "Туслаач"
tr['my_he'] = "ကူညီပါ"
tr['ne_he'] = "मद्दत गर्नुहोस्"
tr['no_he'] = "Hjelp"
tr['or_he'] = "ସାହାଯ୍ୟ"
tr['ps_he'] = "مرسته"
tr['fa_he'] = "کمک"
tr['pl_he'] = "Pomoc"
tr['pt_he'] = "Ajuda"
tr['pa_he'] = "ਮਦਦ ਕਰੋ"
tr['ro_he'] = "Ajutor"
tr['ru_he'] = "Помощь"
tr['sm_he'] = "Fesoasoani"
tr['gd_he'] = "Cuideachadh"
tr['sr_he'] = "Помоћ"
tr['st_he'] = "Thusa"
tr['sn_he'] = "Help"
tr['sd_he'] = "مدد"
tr['si_he'] = "උදව්"
tr['sk_he'] = "Pomoc"
tr['sl_he'] = "pomoč"
tr['so_he'] = "I caawi"
tr['es_he'] = "Ayuda"
tr['su_he'] = "Tulung"
tr['sw_he'] = "Msaada"
tr['sv_he'] = "Hjälp"
tr['tg_he'] = "Кумак"
tr['ta_he'] = "உதவி"
tr['te_he'] = "సహాయం"
tr['th_he'] = "ช่วย"
tr['tr_he'] = "Yardım"
tr['uk_he'] = "Довідка"
tr['ur_he'] = "مدد"
tr['ug_he'] = "ياردەم"
tr['uz_he'] = "Yordam bering"
tr['vi_he'] = "Cứu giúp"
tr['cy_he'] = "Help"
tr['xh_he'] = "Nceda"
tr['yi_he'] = "הילף"
tr['yo_he'] = "Egba Mi O"
tr['zu_he'] = "Usizo"
tr['af_bio'] = "Bioinformatika"
tr['sq_bio'] = "Bioinformatika"
tr['am_bio'] = "ባዮኢንፎርማቲክስ"
tr['ar_bio'] = "المعلوماتية الحيوية"
tr['hy_bio'] = "Կենսաինֆորմատիկա"
tr['az_bio'] = "Bioinformatika"
tr['eu_bio'] = "Bioinformatika"
tr['be_bio'] = "Біяінфарматыка"
tr['bn_bio'] = "বায়োইনফরমেটিক্স"
tr['bs_bio'] = "Bioinformatika"
tr['bg_bio'] = "Биоинформатика"
tr['ca_bio'] = "Bioinformàtica"
tr['ceb_bio'] = "Bioinformatics"
tr['ny_bio'] = "Bioinformatics"
tr['zh_bio'] = "生物信息學"
tr['co_bio'] = "Bioinformatica"
tr['hr_bio'] = "Bioinformatika"
tr['cs_bio'] = "Bioinformatika"
tr['da_bio'] = "Bioinformatik"
tr['nl_bio'] = "Bio-informatica"
tr['en_bio'] = "Bioinformatics"
tr['eo_bio'] = "Bioinformadiko"
tr['et_bio'] = "Bioinformaatika"
tr['tl_bio'] = "Bioinformatics"
tr['fi_bio'] = "Bioinformatiikka"
tr['fr_bio'] = "Bioinformatique"
tr['fy_bio'] = "Bioinformatika"
tr['gl_bio'] = "Bioinformática"
tr['ka_bio'] = "ბიოინფორმატიკა"
tr['de_bio'] = "Bioinformatik"
tr['el_bio'] = "Βιοπληροφορική"
tr['gu_bio'] = "બાયોઇન્ફોર્મેટિક્સ"
tr['ht_bio'] = "Bioenfòmatik"
tr['ha_bio'] = "Bioinformatics"
tr['haw_bio'] = "Bioinformatics"
tr['iw_bio'] = "ביואינפורמטיקה"
tr['he_bio'] = "ביואינפורמטיקה"
tr['hi_bio'] = "बायोइनफॉरमैटिक्स"
tr['hmn_bio'] = "Bioinformatics"
tr['hu_bio'] = "Bioinformatika"
tr['is_bio'] = "Lífupplýsingafræði"
tr['ig_bio'] = "Bioinformatics"
tr['id_bio'] = "Bioinformatika"
tr['ga_bio'] = "Bithfhaisnéisíocht"
tr['it_bio'] = "Bioinformatica"
tr['ja_bio'] = "バイオインフォマティクス"
tr['jw_bio'] = "Bioinformatika"
tr['kn_bio'] = "ಬಯೋಇನ್ಫರ್ಮ್ಯಾಟಿಕ್ಸ್"
tr['kk_bio'] = "Биоинформатика"
tr['km_bio'] = "ជីវព័ត៌មានវិទ្យា"
tr['ko_bio'] = "생물정보학"
tr['ku_bio'] = "Bioinformatics"
tr['ky_bio'] = "Биоинформатика"
tr['lo_bio'] = "ຊີວະວິທະຍາ"
tr['la_bio'] = "Bioinformatics"
tr['lv_bio'] = "Bioinformātika"
tr['lt_bio'] = "Bioinformatika"
tr['lb_bio'] = "Bioinformatik"
tr['mk_bio'] = "Биоинформатика"
tr['mg_bio'] = "Bioinformatics"
tr['ms_bio'] = "Bioinformatik"
tr['ml_bio'] = "ബയോ ഇൻഫോർമാറ്റിക്സ്"
tr['mt_bio'] = "Bijoinformatika"
tr['mi_bio'] = "Bioinformatics"
tr['mr_bio'] = "बायोइन्फॉरमॅटिक्स"
tr['mn_bio'] = "Биоинформатик"
tr['my_bio'] = "ဇီဝအချက်အလက်"
tr['ne_bio'] = "बायोइन्फर्मेटिक्स"
tr['no_bio'] = "Bioinformatikk"
tr['or_bio'] = "ବାୟୋନ୍ଫର୍ମାଟିକ୍ସ |"
tr['ps_bio'] = "بایو انفارماتیک"
tr['fa_bio'] = "بیوانفورماتیک"
tr['pl_bio'] = "Bioinformatyka"
tr['pt_bio'] = "Bioinformática"
tr['pa_bio'] = "ਬਾਇਓਇਨਫੋਰਮੈਟਿਕਸ"
tr['ro_bio'] = "Bioinformatica"
tr['ru_bio'] = "Биоинформатика"
tr['sm_bio'] = "Bioinformatics"
tr['gd_bio'] = "Bith-fiosrachadh"
tr['sr_bio'] = "Биоинформатика"
tr['st_bio'] = "Bioinformatics"
tr['sn_bio'] = "Bioinformatics"
tr['sd_bio'] = "بايو انفارميٽڪس"
tr['si_bio'] = "ජෛව තොරතුරු"
tr['sk_bio'] = "Bioinformatika"
tr['sl_bio'] = "Bioinformatika"
tr['so_bio'] = "Bioinformatics"
tr['es_bio'] = "Bioinformática"
tr['su_bio'] = "Bioinformatika"
tr['sw_bio'] = "Bioinformatics"
tr['sv_bio'] = "Bioinformatik"
tr['tg_bio'] = "Биоинформатика"
tr['ta_bio'] = "உயிர் தகவலியல்"
tr['te_bio'] = "బయోఇన్ఫర్మేటిక్స్"
tr['th_bio'] = "ชีวสารสนเทศศาสตร์"
tr['tr_bio'] = "biyoinformatik"
tr['uk_bio'] = "Біоінформатика"
tr['ur_bio'] = "بایو انفارمیٹکس"
tr['ug_bio'] = "Bioinformatics"
tr['uz_bio'] = "Bioinformatika"
tr['vi_bio'] = "Tin sinh học"
tr['cy_bio'] = "Biowybodeg"
tr['xh_bio'] = "Bioinformatics"
tr['yi_bio'] = "ביאָינפאָרמאַטיקס"
tr['yo_bio'] = "Bioinformatics"
tr['zu_bio'] = "I-Bioinformatics"
tr['af_alrop'] = "DAT Linux Control Panel is already open"
tr['sq_alrop'] = "Paneli i kontrollit DAT Linux është tashmë i hapur"
tr['am_alrop'] = "DAT ሊኑክስ የቁጥጥር ፓነል አስቀድሞ ክፍት ነው።"
tr['ar_alrop'] = "لوحة تحكم DAT Linux مفتوحة بالفعل"
tr['hy_alrop'] = "DAT Linux կառավարման վահանակն արդեն բաց է"
tr['az_alrop'] = "DAT Linux İdarəetmə Paneli artıq açıqdır"
tr['eu_alrop'] = "DAT Linux Kontrol Panela irekita dago jada"
tr['be_alrop'] = "Панэль кіравання DAT Linux ужо адкрыта"
tr['bn_alrop'] = "DAT Linux কন্ট্রোল প্যানেল ইতিমধ্যেই খোলা আছে"
tr['bs_alrop'] = "DAT Linux Control Panel je već otvoren"
tr['bg_alrop'] = "Контролният панел на DAT Linux вече е отворен"
tr['ca_alrop'] = "El tauler de control DAT Linux ja està obert"
tr['ceb_alrop'] = "Ang DAT Linux Control Panel bukas na"
tr['ny_alrop'] = "DAT Linux Control Panel yatsegulidwa kale"
tr['zh_alrop'] = "DAT Linux 控制面板已打開"
tr['co_alrop'] = "DAT Linux Control Panel hè digià apertu"
tr['hr_alrop'] = "Upravljačka ploča DAT Linuxa već je otvorena"
tr['cs_alrop'] = "Ovládací panel DAT Linux je již otevřen"
tr['da_alrop'] = "DAT Linux Kontrolpanel er allerede åbent"
tr['nl_alrop'] = "DAT Linux Control Panel is already open"
tr['en_alrop'] = "DAT Linux Control Panel is already open"
tr['eo_alrop'] = "DAT Linuksa Kontrolpanelo jam estas malfermita"
tr['et_alrop'] = "DAT Linuxi juhtpaneel on juba avatud"
tr['tl_alrop'] = "Bukas na ang DAT Linux Control Panel"
tr['fi_alrop'] = "DAT Linux Control Panel on jo auki"
tr['fr_alrop'] = "Le panneau de configuration DAT Linux est déjà ouvert"
tr['fy_alrop'] = "DAT Linux Control Panel is already open"
tr['gl_alrop'] = "O panel de control DAT Linux xa está aberto"
tr['ka_alrop'] = "DAT Linux პანელი უკვე გახსნილია"
tr['de_alrop'] = "DAT Linux Control Panel ist bereits geöffnet"
tr['el_alrop'] = "Ο Πίνακας Ελέγχου του DAT Linux είναι ήδη ανοιχτός"
tr['gu_alrop'] = "DAT Linux નિયંત્રણ પેનલ પહેલેથી જ ખુલ્લું છે"
tr['ht_alrop'] = "DAT Linux Kontwòl Panel deja louvri"
tr['ha_alrop'] = "DAT Linux Control Panel ya riga ya buɗe"
tr['haw_alrop'] = "Ua wehe ʻia ka DAT Linux Control Panel"
tr['iw_alrop'] = "לוח הבקרה של DAT Linux כבר פתוח"
tr['he_alrop'] = "לוח הבקרה של DAT Linux כבר פתוח"
tr['hi_alrop'] = "DAT Linux नियंत्रण कक्ष पहले से खुला है"
tr['hmn_alrop'] = "DAT Linux Control Vaj Huam Sib Luag twb qhib lawm"
tr['hu_alrop'] = "A DAT Linux vezérlőpult már meg van nyitva"
tr['is_alrop'] = "DAT Linux stjórnborðið er nú þegar opið"
tr['ig_alrop'] = "DAT Linux Control Panel emeghelarị"
tr['id_alrop'] = "Panel Kontrol DAT Linux sudah terbuka"
tr['ga_alrop'] = "Tá Painéal Rialaithe DAT Linux oscailte cheana féin"
tr['it_alrop'] = "Il pannello di controllo di DAT Linux è già aperto"
tr['ja_alrop'] = "DATLinuxコントロールパネルはすでに開いています"
tr['jw_alrop'] = "Panel Kontrol DAT Linux wis mbukak"
tr['kn_alrop'] = "DAT Linux ನಿಯಂತ್ರಣ ಫಲಕ ಈಗಾಗಲೇ ತೆರೆದಿದೆ"
tr['kk_alrop'] = "DAT Linux басқару тақтасы әлдеқашан ашық"
tr['km_alrop'] = "ផ្ទាំងបញ្ជាលីនុច DAT បានបើករួចហើយ"
tr['ko_alrop'] = "DAT Linux 제어판이 이미 열려 있습니다."
tr['ku_alrop'] = "Panela Kontrolê ya DAT Linux jixwe vekirî ye"
tr['ky_alrop'] = "DAT Linux Башкаруу панели мурунтан эле ачык"
tr['lo_alrop'] = "ແຜງຄວບຄຸມ Linux DAT ເປີດຢູ່ແລ້ວ"
tr['la_alrop'] = "DAT Linux Imperium Panel iam aperta"
tr['lv_alrop'] = "DAT Linux vadības panelis jau ir atvērts"
tr['lt_alrop'] = "DAT Linux valdymo skydelis jau atidarytas"
tr['lb_alrop'] = "DAT Linux Kontrollpanel ass scho op"
tr['mk_alrop'] = "Контролната табла DAT Linux е веќе отворена"
tr['mg_alrop'] = "Efa misokatra ny DAT Linux Control Panel"
tr['ms_alrop'] = "Panel Kawalan DAT Linux sudah dibuka"
tr['ml_alrop'] = "DAT Linux നിയന്ത്രണ പാനൽ ഇതിനകം തുറന്നിരിക്കുന്നു"
tr['mt_alrop'] = "DAT Linux Control Panel huwa diġà miftuħ"
tr['mi_alrop'] = "Kua tuwhera kē te Paewhiri Mana Linux DAT"
tr['mr_alrop'] = "DAT Linux नियंत्रण पॅनेल आधीच उघडले आहे"
tr['mn_alrop'] = "DAT Linux Control Panel аль хэдийн нээлттэй байна"
tr['my_alrop'] = "DAT Linux Control Panel ကို ဖွင့်ထားပြီးဖြစ်သည်။"
tr['ne_alrop'] = "DAT Linux नियन्त्रण प्यानल पहिले नै खुला छ"
tr['no_alrop'] = "DAT Linux-kontrollpanelet er allerede åpent"
tr['or_alrop'] = "DAT ଲିନକ୍ସ କଣ୍ଟ୍ରୋଲ୍ ପ୍ୟାନେଲ୍ ଖୋଲା ଅଛି |"
tr['ps_alrop'] = "د DAT لینکس کنټرول پینل لا دمخه خلاص دی"
tr['fa_alrop'] = "کنترل پنل لینوکس DAT در حال حاضر باز است"
tr['pl_alrop'] = "Panel sterowania DAT Linux jest już otwarty"
tr['pt_alrop'] = "O painel de controle do DAT Linux já está aberto"
tr['pa_alrop'] = "DAT Linux ਕੰਟਰੋਲ ਪੈਨਲ ਪਹਿਲਾਂ ਹੀ ਖੁੱਲ੍ਹਾ ਹੈ"
tr['ro_alrop'] = "Panoul de control DAT Linux este deja deschis"
tr['ru_alrop'] = "Панель управления DAT Linux уже открыта"
tr['sm_alrop'] = "DAT Linux Control Panel ua uma ona tatala"
tr['gd_alrop'] = "Tha Pannal Smachd DAT Linux fosgailte mu thràth"
tr['sr_alrop'] = "ДАТ Линук контролна табла је већ отворена"
tr['st_alrop'] = "DAT Linux Control Panel e se e butsoe"
tr['sn_alrop'] = "DAT Linux Control Panel yakatovhurwa"
tr['sd_alrop'] = "DAT Linux ڪنٽرول پينل اڳ ۾ ئي کليل آهي"
tr['si_alrop'] = "DAT Linux පාලන පැනලය දැනටමත් විවෘතයි"
tr['sk_alrop'] = "Ovládací panel DAT Linux je už otvorený"
tr['sl_alrop'] = "Nadzorna plošča DAT Linux je že odprta"
tr['so_alrop'] = "DAT Linux Control Panel mar horeba wuu furan yahay"
tr['es_alrop'] = "El panel de control de DAT Linux ya está abierto"
tr['su_alrop'] = "DAT Linux Control Panel parantos dibuka"
tr['sw_alrop'] = "Paneli ya Kudhibiti ya DAT Linux tayari imefunguliwa"
tr['sv_alrop'] = "DAT Linux kontrollpanel är redan öppen"
tr['tg_alrop'] = "Панели идоракунии DAT Linux аллакай кушода аст"
tr['ta_alrop'] = "DAT லினக்ஸ் கண்ட்ரோல் பேனல் ஏற்கனவே திறக்கப்பட்டுள்ளது"
tr['te_alrop'] = "DAT Linux కంట్రోల్ ప్యానెల్ ఇప్పటికే తెరవబడింది"
tr['th_alrop'] = "แผงควบคุม DAT Linux เปิดอยู่แล้ว"
tr['tr_alrop'] = "DAT Linux Denetim Masası zaten açık"
tr['uk_alrop'] = "Панель керування DAT Linux уже відкрита"
tr['ur_alrop'] = "DAT لینکس کنٹرول پینل پہلے ہی کھلا ہے۔"
tr['ug_alrop'] = "DAT Linux كونترول تاختىسى ئاللىقاچان ئېچىلدى"
tr['uz_alrop'] = "DAT Linux boshqaruv paneli allaqachon ochiq"
tr['vi_alrop'] = "Bảng điều khiển DAT Linux đã được mở"
tr['cy_alrop'] = "Mae Panel Rheoli DAT Linux eisoes ar agor"
tr['xh_alrop'] = "Iqela lenjongo yolawulo ye DAT Linux sele ivuliwe"
tr['yi_alrop'] = "DAT Linux קאָנטראָל פּאַנעל איז שוין אָפֿן"
tr['yo_alrop'] = "Igbimọ Iṣakoso Lainos DAT ti ṣii tẹlẹ"
tr['zu_alrop'] = "Iphaneli yokulawula ye-DAT Linux isivele ivuliwe"

lang = system_lang()
APPS_PATH = '/usr/share/applications/datlinux'
dat_apps_dir = APPS_PATH + "/Data Science Apps/"
dat_extr_dir = APPS_PATH + "/DAT Linux Extras/"
dat_admn_dir = APPS_PATH + "/DAT Linux Admin/"
dat_help_dir = APPS_PATH + "/DAT Linux Help/"

loc_ctrpnl = 'Control Panel'
loc_about = 'About'
loc_distr = 'Distribution'
if lang != 'en':
    loc_ctrpnl = tr[f"{lang}_cp"]
    loc_about = tr[f"{lang}_ab"]
    loc_distr = tr[f"{lang}_di"]
root = ThemedTk(theme="clearlooks", className=f"DAT Linux 1.0.1 {loc_ctrpnl}")
root.title(f"DAT Linux {loc_ctrpnl}")
ico = tk.PhotoImage(file='/usr/share/icons/datlinux/datlinux_logo.png')
root.iconphoto(True, ico)
root.geometry("962x620")
root.resizable(0, 0)

style = ttk.Style()
style.configure("BW.TLabel", foreground="black", background="white")

tabControl = ttk.Notebook(root)
tab1 = ttk.Frame(tabControl)
tab2 = ttk.Frame(tabControl)
tab3 = ttk.Frame(tabControl)
tab4 = ttk.Frame(tabControl)
home = str(Path.home())
loc_vars = {'ds_1' : "Data Science",
            'ds_2' : "Apps",
            'ex' : "Extras",
            'ad' : "Admin",
            'he' : "Help",
            'bio' : 'Bioinformatics'}

if lang != 'en':
    loc_vars['ds_1'] = tr[f"{lang}_ds_1"]
    loc_vars['ds_2'] = tr[f"{lang}_ds_2"]
    loc_vars['ex'] = tr[f"{lang}_ex"]
    loc_vars['ad'] = tr[f"{lang}_ad"]
    loc_vars['he'] = tr[f"{lang}_he"]
    loc_vars['bio'] = tr[f"{lang}_bio"]

tabControl.add(tab1, text=f" {loc_vars['ds_1']}   \n {loc_vars['ds_2']} ")
tabControl.add(tab2, text=f" {loc_vars['ds_1']}   \n {loc_vars['ex']} ")
tabControl.add(tab3, text=f" {loc_vars['ad']}   \n ")
tabControl.add(tab4, text=f" {loc_vars['he']}    \n    ")

tabControl.pack(expand=1, fill="both")
tabControl.grid(column = 1,
               row = 1,
               padx = 0,
               pady = 0,
               sticky=tk.N)
vers=os.popen('apt show datlinux-ctrl-pnl | grep "Version"').read()
footer = tk.Label(root, text=f"{loc_ctrpnl} " + vers,
                  font=('"Ubuntu" 9 normal'), justify=tk.LEFT)
footer.grid(column = 1,
               row = 2,
               padx = 3,
               pady = 4,
               sticky=tk.W)

os.system("mkdir -p " + str(Path.home()) + "/.cache/datlinux/")
myFont = font.Font(family='Ubuntu', size=9, weight="normal")

def status_msg(msg):
    footer['text'] = msg
    time.sleep(3)
    footer['text'] = f"{loc_ctrpnl} " + vers
  
def run_cmd(cmd):
    os.system("nohup " + cmd + " &> /dev/null & sleep 1")

def btn_clicked(cmd, nm):
    x = threading.Thread(target=status_msg,
                         args=('⚙️ ' + nm + '..',))
    x.start()
    y = threading.Thread(target=run_cmd, args=(cmd,))
    y.start()


def about_btn_clicked():
    x = threading.Thread(target=status_msg,
                         args=(f"⚙️ {loc_about}..",))
    x.start()
    y = threading.Thread(target=about_popup)
    y.start()


def create_button(name_, exec_, icon_, desc_, im_pth, tab, reg_bg=True):
    if len(name_.split()) > 1:
        namex_ = name_.replace(" ", "\n ")
    else:
        namex_ = name_ + "\n "

    if not os.path.exists(im_pth):
        im = Image.open(icon_)
        im.resize((60, 60)).save(im_pth)
    img = ImageTk.PhotoImage(file=im_pth)
    if name_ == f"{loc_about}":
        action_with_arg = partial(about_btn_clicked)
    else:
        action_with_arg = partial(btn_clicked, exec_, name_)
    butt_bg = '#FBFBFB'
    butt_fg = '#050505'
    if reg_bg == False:
        butt_bg = '#EFEBE7'
        butt_fg = '#2e3436'
    butt = tk.Button(
        tab,
        fg=butt_fg,
        cursor="hand1",
        bg=butt_bg,
        bd=1,
        width=66,
        highlightthickness=0,
        image=img,
        text=namex_,
        justify=tk.CENTER,
        compound=tk.TOP,
        command=action_with_arg
    )
    butt.cari_exec = None
    try:
        butt.cari_exec = exec_.split('dat-')[1]
        butt.cari_exec = butt.cari_exec.split('-')[0]
    except:
        print('oops.. no _exec found')
    butt.image = img
    butt['font'] = myFont
    Hovertip(butt, textwrap.fill(desc_, 42))
    return butt

app_button_names = {}

def create_tab_buttons(apps, dirr, tab, cols, is_apps=False):
    cari = None
    if is_apps == True:
        cari = os.popen('cari list').read().split("\n")
    btns = []
    col = 0
    row = 0
    lanng = system_lang()
    apps.sort(key=str.lower)
    for f in apps:
        reg_bg = True
        if f.endswith('.pdf'):
            spl = f.split('/')
            name_ = spl[len(spl)-1]
            name_ = name_.replace("DAT Linux FAQ", "DAT-Linux FAQ").replace(".pdf","")
            exec_ = 'xdg-open "' + dirr + f + '"'
            icon_ = '/usr/share/icons/datlinux/pdf.png'
            desc_ = name_
        elif not f.endswith('.desktop'):
            continue
        else:
            appnm = ''
            config = configparser.RawConfigParser()
            config.read(dirr + f)
            name_ = config.get("Desktop Entry", "Name")
            exec_ = config.get("Desktop Entry", "Exec")
            exec_ = re.sub(' %.', '', exec_)
            icon_ = config.get("Desktop Entry", "Icon")
            desc_ = config.get("Desktop Entry", "Comment")
            try:
                if lanng != "en":
                    desc_ = config.get("Desktop Entry", f"Comment[{lanng}]")
            except:
                print(f'No {lanng}!')
                print(dirr + f)
            if is_apps == True:
                app = exec_.split('dat-')[1].split('-')[0]
                app_found = False
                if app == 'pluto':
                    app = 'julia'
                if app == 'openrefine':
                    app = 'refine'
                app_button_names[name_.split(' ')[0]] = app
                for c in cari:
                    if c.startswith(app):
                        appnm = app
                        app_found = True
                        break
                if app_found == False:
                        reg_bg = False
        im_pth = str(Path.home()) + "/.cache/datlinux/" + name_ + " - " + CTRLPNLVER + ".png"
        butt = create_button(name_, exec_, icon_, desc_, im_pth, tab, reg_bg)
            
        butt.grid(column = col,
               row = row+1,
               padx = 2,
               pady = 2,
               sticky=tk.N)

        if col == cols-1:
            col = 0
            row = row + 2
        else:
            col = col + 1

        if not butt.cari_exec is None:
            btns.append(butt)

    if dirr == dat_help_dir:
        print(dirr, dat_help_dir)
        btn = create_button(f"{loc_about}", '', '/usr/share/icons/datlinux/about.png',
        f"About DAT Linux {loc_ctrpnl} {CTRLPNLVER}",
        str(Path.home()) + "/.cache/datlinux/about.png", tab)
        btn.grid(column = col,
               row = row+1,
               padx = 2,
               pady = 2,
               sticky=tk.N)
    
    return btns


def get_configs(dirr):
    ret = []
    files = os.listdir(dirr)
    for f in files:
        ret.append(f)
    return ret

import webbrowser
def webbrowser_callback():
    webbrowser.open_new(f"{REPO}/-/raw/main/README.md")
    
def about_popup():
    top = tk.Toplevel(root)
    top.geometry("730x406")
    top.title(f"{loc_about}")
    msg1_1=os.popen('echo $XDG_CURRENT_DESKTOP').read()
    msg2=os.popen('lsb_release -dc').read()
    msg2_1=os.popen('uname -r').read()
    msg3=f"⚙️ DAT Linux {loc_ctrpnl}:"
    msg4=os.popen('apt show datlinux-ctrl-pnl').read()
    msg=f"""{msg2}Kernel:         {msg2_1}Desktop:        {msg1_1}
{msg3}

{msg4}"""
    tk.Label(top, text=f"💿️ {loc_distr}:", font=('"Ubuntu Mono" 11'),
            justify=tk.LEFT).place(x=8,y=8)
    tk.Label(top, text="DAT Linux 1.0.1-lxqt",
            justify=tk.LEFT).place(x=8,y=32)
    tk.Label(top, text=msg, font=('"Ubuntu Mono" 11'),
            justify=tk.LEFT).place(x=8,y=65)
    tk.Button(
        top,
        fg="dark blue",
        cursor="hand1",
        bd=1,
        width=12,
        highlightthickness=0,
        text="📄️ Release notes",
        justify=tk.CENTER,
        command=webbrowser_callback
    ).place(x=568,y=6)
    

class AppsThread(Thread):
    def __init__(self, event, configs, apps_dir, tab, lengt):
        Thread.__init__(self)
        self.stopped = event
        self.configs = configs
        self.apps_dir = apps_dir
        self.tab = tab
        self.lengt = lengt
    def run(self):
        apps_buttons = create_tab_buttons(self.configs, self.apps_dir, self.tab, self.lengt, True)
        while not self.stopped.wait(30):
            cari = os.popen('cari list').read().split("\n")
            for ab in apps_buttons:
                app_found = False
                for c in cari:
                    k = str(ab['text']).split('\n')[0]
                    if c.startswith(app_button_names[k]):
                        print(app_button_names[k] + ' :: ' + c)
                        if ab['bg'] != '#FBFBFB':
                            ab['bg'] = '#FBFBFB'
                            print('added app:' + c)
                        app_found = True
                        break
                if app_found == False and ab['bg'] != '#EFEBE7':
                        ab['bg'] = '#EFEBE7'
                        print('removed app:' + c)
                
            
stopFlag = Event()
thread = AppsThread(stopFlag, get_configs(dat_apps_dir), dat_apps_dir, tab1, 10)
thread.start()

create_tab_buttons(get_configs(dat_extr_dir), dat_extr_dir, tab2, 10)
create_tab_buttons(get_configs(dat_admn_dir), dat_admn_dir, tab3, 10)
create_tab_buttons(get_configs(dat_help_dir), dat_help_dir, tab4, 10)

def stop(): # stop button to close the gui and should terminate the download function too
    print('stopping')
    try:
        get_lock._lock_socket.close()
    except socket.error:
        None
    stopFlag.set()
    root.destroy()
   
root.protocol('WM_DELETE_WINDOW', stop)
   
root.mainloop()
